<section class="banner">
  <div class="container">
    <div class="banner__wrap">
      <a href="#" class="banner__logo-wrap">
        <img src="./assets/media/img/logo.png" alt="" class="banner__logo">
      </a>
      <img src="./assets/media/img/banner-widget.png" alt="" class="banner__widget">
    </div>
  </div>
</section>